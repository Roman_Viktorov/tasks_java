package HomeTasks;

public class TaskCh09N107 {

    public static void main(String[] args) {
        StringBuilder word = new StringBuilder("абракодобро");
        int numberLetterА = word.indexOf("а");
        int numberLetterО = word.lastIndexOf("о");
        if (numberLetterА >= 0 && numberLetterО >= 0) {
            System.out.println("Первоначальное слово: " + word);
            word = word.replace(numberLetterА, numberLetterА + 1, "о");
            word = word.replace(numberLetterО, numberLetterО + 1, "а");
            System.out.println("Слово после перестановки: " + word);
        } else System.out.println("Обязательное условие не выполняется. В слове нет буквы 'а' или буквы'о'.");


    }
}

