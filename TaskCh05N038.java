package HomeTasks;

public class TaskCh05N038 {
    public static void main(String[] args) {
        double way = 0;       // общий путь
        double distance = 0; //дистанция от дома

        for (int cicle = 1; cicle < 101; cicle++) {
            way = way + (1.0 / cicle);
            if (cicle % 2 == 0) {
                distance = distance - (1.0 / cicle);
            } else if (cicle % 2 != 0) {
                distance = distance + (1.0 / cicle);
            }
        }

        System.out.println("Расстояние до дома " + distance + " км");
        System.out.println("Проделанный общий путь " + way + " км");
    }


}


