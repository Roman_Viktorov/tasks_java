package HomeTasks;

import java.util.Scanner;

public class TaskCh04N115 {


    public static void main(String[] args) {
        int year, yearAnimal, colorАnimal;
        String color, animal;
        Scanner scan = new Scanner(System.in);
        System.out.println("Ведите год:");
        year = scan.nextInt();
        yearAnimal = year % 12;
        colorАnimal = year % 10;

        switch (yearAnimal) {
            case 0:
                animal = "Это год обезьяны";
                break;
            case 1:
                animal = "Это год петуха";
                break;
            case 2:
                animal = "Это год собака";
                break;
            case 3:
                animal = "Это год свиньи";
                break;
            case 4:
                animal = "Это год крысы";
                break;
            case 5:
                animal = "Это год коровы";
                break;
            case 6:
                animal = "Это год тигра";
                break;
            case 7:
                animal = "Это год зайца";
                break;
            case 8:
                animal = "Это год дракона";
                break;
            case 9:
                animal = "Это год змеи";
                break;
            case 10:
                animal = "Это год лошади";
                break;
            case 11:
                animal = "Это год овцы ";
                break;
            default:
                animal = "нет такого года";
        }

        switch (colorАnimal) {
            case 0:
            case 1:
                color = "белого цвета";
                break;
            case 2:
            case 3:
                color = "черного цвета";
                break;
            case 4:
            case 5:
                color = "зеленого цвета";
                break;
            case 6:
            case 7:
                color = "красного цвета";
                break;
            case 8:
            case 9:
                color = "желтого цвета";
                break;
            default:
                color = "нет такого цвета";
        }

        System.out.println(animal + " " + color);

    }
}
