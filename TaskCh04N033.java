package HomeTasks;

import java.util.Scanner;

class TaskCh04N033 {

    public static void main(String[] args) {
        int c, b;
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите натуральное число. Если оно заканчивается четной цифрой то выводится -true, если нет -false");
        c = scan.nextInt();
        System.out.println(c % 2 == 0);
    }
}
