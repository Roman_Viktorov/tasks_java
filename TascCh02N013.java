package HomeTasks;

import java.util.Scanner;

public class TascCh02N013 {
    public static void main(String[] args) {
        int result;
        int n, a, b;
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите любое число больше 100 и меньше 200: ");
        n = scan.nextInt();
        if (n > 100 & n < 200) {
            a = n % 100;
            b = a % 10;
            result = (b * 100) + (a / 10) * 10 + (n - a) / 100;
            System.out.print("Данное число наоборот: " + result);
        } else System.out.print("Необходимо использовать только число больше 100 и меньше 200! ");


    }
}