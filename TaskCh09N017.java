package HomeTasks;

import java.util.Scanner;

public class TaskCh09N017 {


    public static void main(String[] args) {
        String equalLetters = new String("resulr");
        boolean start, end, result;

        start = equalLetters.startsWith("r");
        end = equalLetters.endsWith("r");
        result = start && end;

        System.out.println("Если первая и последняя буква данного слова совпадают(true), если нет (false) ");
        System.out.println("Результат: " + result);

    }
}

