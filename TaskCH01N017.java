package HomeTasks;

import java.util.Scanner;

public class TaskCH01N017 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double a, b, c, x, y;
        System.out.println("Задание 1.17 (o)");
        System.out.println("Ведите ниже значение x:");
        x = scan.nextDouble();
        y = Math.sqrt(1 - 2 * Math.sin(x));
        System.out.println("Результат решения: " + y);
        System.out.println();

        System.out.println("Задание 1.17 (п)");
        System.out.println("Ведите ниже значение a:");
        a = scan.nextDouble();
        System.out.println("Ведите ниже значение x:");
        x = scan.nextDouble();
        System.out.println("Ведите ниже значение b:");
        b = scan.nextDouble();
        System.out.println("Ведите ниже значение c:");
        c = scan.nextDouble();
        y = 1 / Math.sqrt(a * Math.pow(x, 2) + b * x + c);
        System.out.println("Результат решения: " + y);
        System.out.println();

        System.out.println("Задание 1.17 (р)");
        System.out.println("Ведите ниже значение x:");
        x = scan.nextDouble();
        y = (Math.sqrt(x + 1) + Math.sqrt(x - 1)) / (2 * Math.sqrt(x));
        System.out.println("Результат решения: " + y);
        System.out.println();

        System.out.println("Задание 1.17 (c)");
        System.out.println("Ведите ниже значение x:");
        x = scan.nextDouble();
        y = (Math.abs(x) + Math.abs(x + 1));
        System.out.println("Результат решения: " + y);
    }
}
