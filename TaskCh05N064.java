package HomeTasks;

public class TaskCh05N064 {

    void population() {
        int numberOfPeople[] = {100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100};//население районов
        int area[] = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21};// площадь районов
        int k, i, j, result = 0;
        for (i = 0, j = 0; i < numberOfPeople.length & j < area.length; i++, j++)
            result = result + (numberOfPeople[i] * area[j]);
        System.out.println("Плотность населения в 12 районах: " + result + " человек");
    }

    public static void main(String[] args) {
        TaskCh05N064 report = new TaskCh05N064();
        report.population();
    }
}
