package HomeTasks;

import java.util.Scanner;

public class TaskCH04N036 {
    public static void main(String[] args) {
        int c, b;
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите сколько минут прошло с начала часа:");
        c = scan.nextInt();
        b = c % 5;
        if (b > 0 & b <= 3) System.out.println("Горит зеленый свет");
        else if (b == 0 || b > 3 & b <= 5) System.out.println("Горит красный свет");
    }
}
