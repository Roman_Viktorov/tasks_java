package HomeTasks;

import java.util.Scanner;

public class TaskCh04N106 {
    public static void main(String[] args) {
        int month;
        String season;
        Scanner scan = new Scanner(System.in);
        System.out.println("Ведите номер месяца года");
        month = scan.nextInt();
        switch (month) {
            case 12:
            case 1:
            case 2:
                season = "Месяц относится к сезону зима";
                break;
            case 3:
            case 4:
            case 5:
                season = "Месяц относится к сезону весна";
                break;
            case 6:
            case 7:
            case 8:
                season = "Месяц относится к сезону лето";
                break;
            case 9:
            case 10:
            case 11:
                season = "Месяц относится к сезону осень";
                break;
            default:
                season = "Такого месяца нет";
        }
        System.out.println(season);


    }}
