package HomeTasks;

public class TaskCh10N141 {
    int fact(int n) {
        int result;
        if (n == 1) return 1;
        result = fact(n - 1) * n;
        return result;
    }
}

class New {
    public static void main(String[] args) {
        TaskCh10N141 factorial = new TaskCh10N141();
        System.out.println("Факториал 5 равен-" + factorial.fact(5));
    }
}



