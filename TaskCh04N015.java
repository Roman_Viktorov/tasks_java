package HomeTasks;

import java.util.Scanner;

public class TaskCh04N015 {

    public static void main(String[] args) {
        int monthToday, yearToday, monthBirth, yearBirth, yearsOld;
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите текущий год:");
        yearToday = scan.nextInt();
        System.out.println("Введите текущий месяц:");
        monthToday = scan.nextInt();
        System.out.println("Введите год вашего рождения: ");
        yearBirth = scan.nextInt();
        System.out.println("Введите месяц вашего рождения: ");
        monthBirth = scan.nextInt();

        if (monthToday < monthBirth) {
            yearsOld = yearToday - yearBirth - 1;
            System.out.println("Вам полных " + yearsOld + " лет");
        } else if (monthBirth <= monthToday) {
            yearsOld = yearToday - yearBirth;
            System.out.println("Вам полных " + yearsOld + " лет");
        }
    }
}
