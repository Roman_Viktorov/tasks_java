package HomeTasks;

import java.util.Scanner;

public class TaskCh09N013 {


    public static void main(String[] args) {
        System.out.println("Введите слово");
        Scanner scan = new Scanner(System.in);
        String word = scan.next();

        System.out.println("Введите номер символа:");
        int numberLetter = scan.nextInt();

        char result = word.charAt(numberLetter);
        System.out.println(numberLetter + " символ данного слова- " + result);

    }
}