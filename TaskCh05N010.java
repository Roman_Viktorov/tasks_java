package HomeTasks;

import java.util.Scanner;

public class TaskCh05N010 {

    public static void main(String[] args) {
        float exchange, course;
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите текущий курс доллара:");
        course = scan.nextFloat();
        for (int i = 1; i <= 20; i++) {
            exchange = course * i;
            System.out.println("Стоимость " + i + " долларов составляет: " + exchange + " рублей");
        }
    }
}

