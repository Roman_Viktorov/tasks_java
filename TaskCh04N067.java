package HomeTasks;

import java.util.Scanner;

public class TaskCh04N067 {
    public static void main(String[] args) {
        int day;
        Scanner scan = new Scanner(System.in);
        System.out.println("Номер дня с начала года:");
        day = scan.nextInt();
        if (day < 365) {
            if (day % 7 == 0 || (day + 1) % 7 == 0) System.out.println("Этот день выходной");
            else System.out.println("Этот день рабочий");
        }

    }

}
