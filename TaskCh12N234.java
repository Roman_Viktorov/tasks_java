package HomeTasks;

import java.util.Scanner;

public class TaskCh12N234 {
    public static void main(String[] args) {

        int[][] array = {{1, 2, 3, 4, 5}, {2, 2, 3, 4, 5}, {3, 2, 3, 4, 5}, {4, 2, 3, 4, 5}, {5, 2, 3, 4, 5}};
        
        System.out.println("Первоначальный массив:");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("Вы хотите удалить столбец или строку? Если столбец то введите- 1, если строку, введите- 2");
        Scanner scan = new Scanner(System.in);
        int k = scan.nextInt();
        if (k == 1) {
            System.out.println("Введите номер столбца");
            int column = scan.nextInt();
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array[i].length; j++) {
                    if (j >= column - 1 && j < array[i].length - 1) array[i][j] = array[i][j + 1];
                    else if (j == array[i].length - 1) array[i][j] = 0;
                    System.out.print(array[i][j] + " ");
                }
                System.out.println();

            }
        } else if (k == 2) {
            System.out.println("Введите номер строки");
            int line = scan.nextInt();
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array[i].length; j++) {
                    if (i >= line - 1 && i < array.length - 1) array[i][j] = array[i + 1][j];
                    else if (i == array.length - 1) array[i][j] = 0;
                    System.out.print(array[i][j] + " ");
                }
                System.out.println();

            }
        }
    }
}