package HomeTasks;

import java.util.Scanner;

public class TaskCh06N087Game {
    String team1, team2, count, resultGame;
    int score = 0;
    int teamSelection;
    int resultTeam1 = 0;
    int resultTeam2 = 0;


    public void teams() {

        System.out.println("Enter team #1");
        Scanner scan = new Scanner(System.in);
        team1 = scan.next();
        System.out.println("Enter team #2");
        team2 = scan.next();
    }

    void play() {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter team to score (1 or 2 or 0 to finish game)");
        teamSelection = scan.nextInt();
        if (teamSelection != 0) {
            System.out.println("Enter score (1 or 2 or 3 ) ");
            score = scan.nextInt();
        }
    }

    String score(int teamSelectionIn, int scoreIn) {
        if (teamSelectionIn == 1)
            resultTeam1 = resultTeam1 + scoreIn;
        else if (teamSelectionIn == 2)
            resultTeam2 = resultTeam2 + scoreIn;
        count = "Результат игры: " + team1 + "-" + resultTeam1 + " : " + team2 + "-" + resultTeam2;
        System.out.println(count);
        System.out.println();
        return count;
    }

    String result(String countIn, int resultTeamIn1, int resultTeamIn2) {
        if (resultTeamIn1 > resultTeamIn2) resultGame = "Победила команда- " + team1 + "! " + countIn;
        else if (resultTeamIn2 > resultTeamIn1) resultGame = "Победила команда- " + team2 + "! " + countIn;
        else if (resultTeamIn2 == resultTeamIn1) resultGame = "Ничья! " + countIn;
        System.out.println("Игра завершена! " + resultGame);
        System.out.println();
        return resultGame;
    }
}


class Main {
    public static void main(String[] args) {
        TaskCh06N087Game start = new TaskCh06N087Game();

        start.teams();
        do {
            start.play();
            if (start.teamSelection == 1 || start.teamSelection == 2) {
                start.score(start.teamSelection, start.score);
            } else if (start.teamSelection == 0)
                start.result(start.count, start.resultTeam1, start.resultTeam2);
        }
        while (start.teamSelection != 0);

    }
}



