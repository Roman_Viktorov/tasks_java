package HomeTasks;

import java.util.Scanner;

public class TascCh02N043 {
    public static void main(String[] args) {
        int a, b, c;
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите значение а");
        a = scan.nextInt();
        System.out.println("Введите значение b");
        b = scan.nextInt();
        c = ((a % b) * (b % a)) + 1;

        System.out.println("Если 'a' делится без остатка на 'b' или 'b' на 'a', то результат- 1, иначе другое число.");
        System.out.println("Результат: " + c);
    }
}
