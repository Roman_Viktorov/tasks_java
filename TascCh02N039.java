package HomeTasks;

import java.util.Scanner;

public class TascCh02N039 {
    public static void main(String[] args) {
        double degrees, h, s, m;
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите текущий час:");
        h = scan.nextInt();
        System.out.println("Введите  минуты:");
        m = scan.nextInt();
        System.out.println("Введите секунды:");
        s = scan.nextInt();

        if (0 < h && h <= 12 && 0 <= m && m <= 59 && 0 <= s && s <= 59) {
            degrees = (h * 60 + m + s / 60) * 0.5;                              // Все переводим в минуты. Между 2 часами - 30 градусов, смещение стрелки на 1 минуту=0.5 градусов
            System.out.println("Часовая стрелка отклонилась на " + degrees + " градусов от положения в начале суток");
        } else if (12 < h && h <= 23 && 0 <= m && m <= 59 && 0 <= s && s <= 59) { // рассчет если время после обеда
            degrees = ((h - 12) * 60 + m + s / 60) * 0.5;
            System.out.println("Часовая стрелка отклонилась на " + degrees + " градусов от положения в начале суток");
        } else System.out.println("Введите правильное время.");
    }
}
